from flask import Flask
from flask import Flask, render_template, request, send_from_directory
from werkzeug.utils import secure_filename
import os
import pickle
from itertools import groupby

app: Flask = Flask(__name__)

app.config['DATAFOLDER'] = "datacache/"

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/uploadedFiles/', methods=['GET', 'POST'])
def upload_file():
    uploaded_files = request.files.getlist("file")

    for file in uploaded_files:
        if file:
            # Make the filename safe, remove unsupported chars
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['DATAFOLDER'], filename))
            # Save the filename into a list, we'll use it later

    filenames = os.listdir("datacache")

    return render_template('uploadedFiles.html',filenames=filenames)


@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['DATAFOLDER'], filename)

@app.route('/calculateSkew/', methods=['GET', 'POST'])
def calculateSkew():

    with open("Predictions.pkl", "rb") as f:
        predictions = pickle.load(f)
    n=len(predictions)

    numeric_pred = []

    for i in predictions:
        if(i=="VQA"):
            numeric_pred.append(0)
        elif(i=='VizWiz'):
            numeric_pred.append(1)
        else:
            numeric_pred.append(2)

    # freq = [len(list(group)) for key, group in groupby(a)]


    ## Load the Model

    ## Classify the dataset

    ## print the results

    ## Show it on the App

    return render_template('skewCalculation.html', numeric_pred=numeric_pred)

if __name__ == '__main__':
    app.run(debug=True)